from .. import message, comm
from ..constants import Tag

import unittest
import datetime


class TestMessage(unittest.TestCase):


    def test_parts(self):
        
        self.assertListEqual(
            [
                [b"1=a\x01",b"10=1\x01"],
                [b"2=bb\x01",b"22=10=2\x01",b"10=2\x01"]
            ], 
            list(message.Message._parts(comm.MockSocket(
                b"1=a\x0110=1\x012=bb\x012", b"2=10=2\x0110=2\x01",
                b"3=c" #ignored
            )))
        )
        
    def test_checksum(self):
        # Test with real message sent by ROFEX demo 
        self.assertEqual(
            79, 
            message.Message._checksum(b"8=FIXT.1.1\x01",b"9=75\x01",b"35=A\x01",b"34=1\x01",b"49=ROFX\x01",b"52=20170417-18:29:09.599\x01",b"56=eco\x0198=0\x01",b"108=20\x01",b"141=Y\x01",b"1137=9\x01")
        )
        
    def test_empty_message(self):
        self.assertListEqual([], list(message.Message()))
        
    def test_from_parts(self):
        m = message.Message((1,b"a"), (2,b"bb"))
        self.assertListEqual([(1,b"a"), (2,b"bb")], list(m))
        self.assertEqual((2,b"bb"), m[1])
        self.assertEqual(2, len(m))
        
    def test_invalid_part(self):
        self.assertRaises(message.InvalidPart, message.Message, "Not a tuple")
        self.assertRaises(message.InvalidPart, message.Message, (1, b"a", b"b"))
        self.assertRaises(message.InvalidPart, message.Message, (1, u"a"))
        self.assertRaises(message.InvalidPart, message.Message, (1, 2))
        self.assertRaises(message.InvalidPart, message.Message, (b"a", b"a"))
        self.assertRaises(message.InvalidPart, message.Message, (1, b"a\x01b"))
        
    def test_append(self):
        m = message.Message((1,b"a"), (2,b"bb"))
        m.append(3, b"ccc")
        self.assertEqual((3,b"ccc"), m[2])
        self.assertEqual(3, len(m))
        
    def test_add(self):
        m1 = message.Message((1,b"a"), (2,b"bb"))
        m2 = message.Message((3,b"ccc"), (4, b"dddd"))

        self.assertListEqual(
            [(1,b"a"), (2,b"bb"), (3,b"ccc"), (4, b"dddd")],
            list(m1 + m2)
        )
        
    def test_getitem(self):
        m = message.Message((1,b"a"), (2,b"bb"), (3, b"ccc"))
        
        self.assertEqual((2,b"bb"), m[1])
        self.assertEqual(message.Message((2,b"bb"), (3, b"ccc")), m[1:])

        
    def test_eq(self):
        m1 = message.Message((1,b"a"), (2,b"bb"))
        m2 = message.Message((3,b"ccc"), (4, b"dddd"))
        m3 = message.Message((1,b"a"), (2,b"bb"))
        
        self.assertNotEqual(m1, m2)
        self.assertEqual(m1, m3)
        
    def test_from_bytes(self):
        body = b"1=a\x01"
        header = b"8=AAA\x019=%d\x01" % len(body)
        checksum = message.Message._checksum(header + body)
        self.assertListEqual(
            [message.Message((1,b"a"))],
            list( message.Message.from_bytes(
                header+body+b"10="+str(checksum).encode('ascii')+b"\x01", 
                begin_string=b'AAA'
            ))
        )
        
    def test_error_management_reading_messages(self):
        
        with self.assertRaises(message.InvalidMessage): # short message
            list(message.Message.from_bytes(b"10=000\x01"))
            
        with self.assertRaises(message.InvalidMessage): # no begin string
            list(message.Message.from_bytes(b"9=4\x0110=000\x01"))
            
        with self.assertRaises(message.InvalidMessage): #wrong length
            body = b"1=a\x01"
            header = b"8=FIXT.1.1\x019=%d\x01" % (len(body) + 4)
            checksum = message.Message._checksum(header + body)
            list(message.Message.from_bytes(header+body+b"10="+str(checksum).encode('ascii')+b"\x01"))
        
        with self.assertRaises(message.InvalidMessage): #invalid checksum
            body = b"1=a\x01"
            header = b"8=FIXT.1.1\x019=%d\x01" % len(body)
            checksum = message.Message._checksum(header + body)
            list(message.Message.from_bytes(header+body+b"10="+str(checksum+1).encode('ascii')+b"\x01"))

    def test_to_buf(self):
        m = message.Message((1,b"a"), (2,b"e\xe9"))
        ms = list( message.Message.from_bytes( 
            m.to_buf(begin_string=b'BBB'),
            begin_string=b'BBB' 
        ) ) 
        
        self.assertEqual(1, len(ms))
        self.assertEqual((1,b"a"), ms[0][0])
        self.assertEqual((2,b"e\xe9"), ms[0][1])

    def test_pprint(self):
        self.assertEqual(
            u'MsgType (35): Heartbeat (0)\nUnknown (123456): unhandled\nCheckSum (10): 12\n', 
            message.Message((35, b'0'), (123456,b'unhandled'), (10, b'12')).pprint()
        )
        self.assertEqual(
            b'MsgType (35): Heartbeat (0)\nUnknown (123456): E\xe9\n'.decode('iso8859-1'), 
            message.Message((35, b'0'), (123456,b'E\xe9')).pprint()
        )
        

    def test_get_field(self):
        m = message.Message((1,b"a"),(1,b"b"),(2,b"c"))
        
        self.assertEqual(b"c", m.get_field(2))
        self.assertEqual(b"c", m.get_field(2, b"default"))
        self.assertRaises(message.MultipleTags, m.get_field, 1)
        self.assertRaises(message.TagNotFound, m.get_field, 3)
        self.assertEqual(b"default", m.get_field(3, b"default"))
        
        
    def test_group(self):
        
        g = message.Message.group(
            10,
            message.Message((100,b"a"),(101,b"b")),
            message.Message((100,b"c"),(101,b"d")),
        )
        
        self.assertEqual(
            message.Message((10,b"2"),(100,b"a"),(101,b"b"),(100,b"c"),(101,b"d")),
            g
        )
        
    def test_get_groups(self):
        
        m = message.Message((1,b"a")) + message.Message.group( 10,
            message.Message((100,b"a1")),
            message.Message((100,b"b1"),(101,b"b2"),(102,b"b3")),
            message.Message((100,b"c1"),(101,b"c2")),
            message.Message((100,b"d1"))
        ) + message.Message((2,b"b"))
        
        self.assertListEqual(
            [
                message.Message((100,b"a1")),
                message.Message((100,b"b1"),(101,b"b2"),(102,b"b3")),
                message.Message((100,b"c1"),(101,b"c2")),
                message.Message((100,b"d1"))
            ],
            m.get_groups(10, 100, [101,102])
        )
        
        m = message.Message((1,b"a")) + message.Message.group( 10,
            message.Message((100,b"a1")),
            message.Message((100,b"b1"),(101,b"b2"))
        )
        
        self.assertListEqual(
            [
                message.Message((100,b"a1")),
                message.Message((100,b"b1"),(101,b"b2")),
            ],
            m.get_groups(10, 100, [101,102])
        )
        
        m = message.Message((146, b'1'), (55, b'MERV - XMEV - PBRC38.4AG - 24hs'), (461, b'ESXXXX'))
        self.assertListEqual(
            [
                message.Message((55,b'MERV - XMEV - PBRC38.4AG - 24hs'))
            ], m.get_groups(146, 55, [207])
        )
    
    def test_get_group_all_inclusive(self):
        g1 = message.Message((2,b'b'),(3,b'c'))
        g2 = message.Message((2,b'd'),(3,b'e'),(4,b'f'))
        m = message.Message.group(1, g1, g2 )
        
        self.assertListEqual(
            [g1, g2], 
            m.get_groups(1, 2, message.AllInclusive)
        )

    def test_invalid_group(self):
        m = message.Message((1,b"a")) + message.Message.group( 10,
            message.Message((100,b"a"),(101,b"b"),(2,b"c")),
            message.Message((100,b"c"),(101,b"d")),
        ) + message.Message((2,b"b"))

        self.assertRaises(message.InvalidGroup, m.get_groups, 10, 100, [101])
        
    def test_confidential_password(self):
        m = message.Message((Tag.Password, b'SECRET'), (1,b'ANOTHER'),)
        
        self.assertIn('ANOTHER', repr(m))
        self.assertNotIn('SECRET', repr(m))
        self.assertIn('ANOTHER', m.pprint())
        self.assertNotIn('SECRET', m.pprint())
        
        eval(repr(m), dict(Message=message.Message)) # eval still works
        
        
class TestUtils(unittest.TestCase):
    
    def test_bool(self):
        self.assertEqual(b'Y', message.from_bool(message.to_bool(b'Y')))
        self.assertEqual(b'N', message.from_bool(message.to_bool(b'N')))
        
    def test_int(self):
        self.assertEqual(b'123', message.from_int(message.to_int(b'123')))
        
    def test_decimal(self):
        self.assertEqual(b'1.1', message.from_decimal(message.to_decimal(b'1.1')))
        
    def test_str(self):
        self.assertEqual(b'a', message.from_str(message.to_str(b'a')))
        self.assertEqual(b'\xc3\xa1', message.from_str(message.to_str(b'\xc3\xa1', encoding="utf-8"),encoding="utf-8"))
        
    def test_timestamp(self):
        self.assertEqual(
            b'20000102-03:04:05.006', 
            message.timestamp( datetime.datetime(2000, 1, 2, 3, 4, 5, 6000) )
        )
        message.timestamp() # generates current UTC timestamp
        
    def test_all_inclusive(self):
        self.assertIn(object(), message.AllInclusive)
        