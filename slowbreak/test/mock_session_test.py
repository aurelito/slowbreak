from ..mock_session import MockSessionApp
from .. import app
from ..message import Message

import unittest


class Test(unittest.TestCase):


    def test_mock_session(self):
        
        expected_msgs_in = [Message((35,b"A")), Message((35,b"B")), Message((35,b"C"))]
        initial_msgs_in= expected_msgs_in[:2]
        
        class TestApp(app.BaseApp):
            
            def __init__(self, msgs_in, test, *args, **kwargs):
                
                self.msgs_in = msgs_in[:] # Make a copy as it will be destroyed
                self.test = test
                
                super(TestApp, self).__init__(*args, **kwargs)
            
            def on_msg_in(self, message):
                self.test.assertEqual(message, self.msgs_in[0])
                self.msgs_in = self.msgs_in[1:]
                
                self.send(Message((35, message[0][1] * 2))) # Send a message doubling the field 35
                return message
            
        stack = app.stack(
            (
                MockSessionApp, dict(
                    msgs_in = initial_msgs_in
                )
            ),
            (
                TestApp, dict(
                    msgs_in = expected_msgs_in,
                    test = self
                )
            )
        )
        
        self.assertEqual(1, len(stack.upper_app.msgs_in))
        self.assertListEqual(
            [ b"AA", b"BB"], 
            [ m[0][1] for m in stack.sent_messages ]
        )
        
        stack.clear_sent_messages()
        stack.msg_in(Message((35,b"C")))
        self.assertListEqual(
            [ b"CC" ], 
            [ m[0][1] for m in stack.sent_messages ]
        )
        
        
        

