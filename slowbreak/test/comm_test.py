import unittest

from ..comm import MockSocket
import ssl

class Test(unittest.TestCase):


    def test_use_mock_socket(self):
        s = MockSocket(b"a", None, b"b")
        
        self.assertEqual(b"a", s.read())
        self.assertRaises(ssl.SSLError, s.read)
        self.assertEqual(b"b", s.read())
        self.assertEqual(b"", s.read())
        
        s.sendall(b"A")
        s.sendall(b"BB")
        
        self.assertEqual([b"A", b"BB"], s.written_parts)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()