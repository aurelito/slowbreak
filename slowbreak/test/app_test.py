import unittest
from ..app import BaseApp, ByMessageTypeApp, stack, on
from ..message import Message
from collections import Counter


class TestBaseApp(unittest.TestCase):
    def test_on_msg_in(self):
        EXPECTED_IN = "IN MESSAGE"
        other_self = self

        class Below(BaseApp):
            def on_msg_in(self, message):
                return message

        class Above(BaseApp):
            def on_msg_in(self, message):
                other_self.assertEqual(EXPECTED_IN, message)
                return message

        stack_ = stack((Below, {}), (Above, {}))

        self.assertEqual(EXPECTED_IN, stack_._on_msg_in(EXPECTED_IN))

    def test_stop_on_msg_in(self):
        NOT_BUBBLING = "NOT BUBBLING"
        other_self = self

        class Below(BaseApp):
            def on_msg_in(self, _message):
                return None

        class Above(BaseApp):
            def on_msg_in(self, _message):
                other_self.fail("No bubbling expected")

        stack_ = stack((Below, {}), (Above, {}))
        self.assertIsNone(stack_._on_msg_in(NOT_BUBBLING))
        
    def test_send_single_fix_message(self):
        m = Message((1,b'a'))
        app = BaseApp()
        
        # a single fix message generates a batch of one
        self.assertListEqual(
            [m], 
            list(app.send(m))
        ) 

    def test_send(self):
        class Bottom(BaseApp):
            def on_send_request(self, message):
                return message + " TWICE"

        class Below(BaseApp):
            def on_batch_send_request(self, batch):
                return ["MODIFIED " + batch[1]] # Filters out first message!

        class Above(BaseApp):
            pass

        stack_ = stack((Bottom, {}), (Below, {}), (Above, {}))

        self.assertListEqual(
            ["MODIFIED ANOTHER TWICE"],
            list(stack_.upper_app.send(["OUT MESSAGE","ANOTHER"]))
        )

    def test_block_send(self):
        class Below(BaseApp):
            def on_send_request(self, _message):
                return "Message should have been blocked"

        class Above(BaseApp):
            def on_send_request(self, _message):
                return None

        stack_ = stack((Below, {}), (Above, {}))

        self.assertFalse(stack_.upper_app.send(["OUT MESSAGE"]))

    def test_lower_app(self):
        class Below(BaseApp):
            pass

        class Above(BaseApp):
            pass

        stack_ = stack((Below, {}), (Above, {}))

        self.assertIs(stack_, stack_.upper_app.lower_app)
        
    def test_app_by_name(self):
        
        class L1(BaseApp): pass
        class L2(BaseApp): pass
        class L3(BaseApp): pass
        class L4(BaseApp): pass
        
        stack_ = stack(
            (L1, dict(name='n1')), 
            (L2, dict()),
            (L3, dict(name='n1')),
            (L4, dict(name='n2')),
        )
        
        self.assertIs( stack_.app_by_name('n1'), stack_) # If repeated, returns the lower one
        l4 = stack_.app_by_name('n2')
        self.assertIsInstance( l4, L4 )
        self.assertIsNone( stack_.app_by_name('not registered') )
        
        self.assertIsNone( l4.app_by_name('n1') ) #only children

    def test_on_msg_not_rcvd(self):
        class Below(BaseApp):
            pass

        class Above(BaseApp):
            def __init__(self, *args, **kwargs):
                super(Above, self).__init__(*args, **kwargs)
                self.not_sent_messages = []

            def on_msg_not_rcvd(self, message):
                self.not_sent_messages.append(message)

        stack_ = stack((Below, {}), (Above, {}))

        m = "NOT SENT"

        stack_._on_msg_not_rcvd(m)

        self.assertListEqual([m], stack_.upper_app.not_sent_messages)


class TestByMessageTypeApp(unittest.TestCase):
    def test_message_dispatching(self):
        class MockApp(ByMessageTypeApp):
            def __init__(self, *args, **kwargs):

                self.counter = Counter()
                super(MockApp, self).__init__(*args, **kwargs)

            @on(b"a")
            def on_a(self, _message):
                self.counter[b"a"] += 1
                
            def on_unhandled(self, _message):
                self.counter[b"unhandled"] += 1

        m = MockApp()

        m._on_msg_in(Message((35, b"a")))
        m._on_msg_in(Message((35, b"b")))

        self.assertEqual(1, m.counter[b"a"])
        self.assertEqual(0, m.counter[b"b"])
        self.assertEqual(1, m.counter[b"unhandled"])
