from .. mock_socket import MockSocketPair

import unittest

class MyException(Exception): pass

class Test(unittest.TestCase):


    def testBasicUsage(self):
        
        msp = MockSocketPair()
        s1 = msp.a
        s2 = msp.b
        
        s1.sendall(b"abc")
        s2.add_read_error(MyException())
        s1.sendall(b"de")
        
        self.assertEqual(b"abc", s2.read())
        self.assertRaises(MyException, s2.read)
        self.assertEqual(b"de", s2.read())
        
        s2.sendall(b"ABC")
        s2.sendall(b"DE")
        
        self.assertEqual(b"ABC", s1.read())
        self.assertEqual(b"DE", s1.read())

        s1.close()
        
        self.assertEqual(b"", s1.read())
        self.assertEqual(b"", s2.read())
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()