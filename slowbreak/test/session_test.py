import unittest
from ..session import OutMessageStore, InitiatorSessionApp, AcceptorSessionApp
from ..message import Message, TagNotFound, from_int, to_int 
from ..constants import MsgType
from ..app import BaseApp, stack
from ..mock_socket import MockSocketPair


import ssl
import collections
import six

if False: #True: #  
    import logging
    import sys
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(asctime)s - %(threadName)s - %(name)s - %(levelname)s - %(message)s")



class OutMessageStoreTest(unittest.TestCase):
    
    def test_normal_usage(self):
        
        def check_included(m1, m2):
            for n,v in m1:
                self.assertEqual(v, m2.get_field(n))

        
        def add_and_check(oms, m, seq_num):
            d = oms.decorate_and_register(m)

            check_included(m, d)                
            self.assertEqual(seq_num, to_int(d.get_field(34)))
            
            self.assertEqual(d, oms.get(seq_num))
            self.assertEqual(seq_num + 1, oms.next_seq_num)
            
        oms = OutMessageStore()
        
        add_and_check(oms, Message((35,b"ZZ"),(1,b"a")), 1)
        add_and_check(oms, Message((35,b"ZZ"),(1,b"b")), 2)
        add_and_check(oms, Message((35,b"ZZ"),(1,b"c")), 3)
        add_and_check(oms, Message((35,b"ZZ"),(1,b"d")), 4)
        
        self.assertEqual(4, len(oms))
        
        
        oms.drop(2) # drop messages 1 and 2
        
        self.assertEqual(2, len(oms))
        
        self.assertRaises(IndexError, oms.get, 2)
        self.assertRaises(IndexError, oms.get, 5)
        
        self.assertEqual(b"3", oms.get(3).get_field(34)) # Still has the other elements

class DataCheckMixIn(object):       

    class TestDatum(object):
        def __init__(self, test, expected_fields=(), action=None):
            self.test = test
            self.expected_fields = expected_fields
            self.action = action
            
        def check(self, message):
            for n,v in self.expected_fields:
                self.test.assertEqual(v, message.get_field(n))
                
            if self.action:
                self.action() 
    
    def check_data(self, msp, data):
        
        i = 0
        for m in Message.from_socket(msp.b):
            data[i].check(m)

            i+=1
            
            # avoid race condition when action sends a new message
            if i >= len(data):
                break
            
    def compose(self, *args):
        def composed():
            for f in args:
                f()
                
        return composed
    
    def do_nothing(self):
        return lambda: None
    
    def add_messages(self, msp, *args):
        def add_messages_action():
            for m in args:
                msp.b.sendall(m.to_buf())
                
        return add_messages_action
                
    def add_error(self, msp, exception):
        def add_error_action():
            msp.a.add_read_error(exception)
            
        return add_error_action

class TestAcceptorSessionApp(unittest.TestCase, DataCheckMixIn):
    
    def initiator_logon_msg(self, seq_num=b"1", sender_comp_id=b"INITIATOR", encrypt_method=b"0", heart_bt_int=from_int(10)):
        return Message(
            (35, MsgType.Logon),
            (34, seq_num),
            (49, sender_comp_id),
            (98, encrypt_method),
            (108, heart_bt_int)
        )
        
    def check_invalid_logon_msg(self, message):
        msp = MockSocketPair()
        app = AcceptorSessionApp(
            socket=msp.a,
            we=b"ACCEPTOR"
        )
        msp.b.sendall(message.to_buf())
        self.check_data(
            msp, 
            [self.TestDatum(
                test=self,
                expected_fields=((35,MsgType.Reject),)
            )]
        )
        
        app.wait()

    def test_invalid_encryption_method_on_logon(self):
        self.check_invalid_logon_msg(self.initiator_logon_msg(encrypt_method=b"OTHER"))
        
    def test_valid_logon_lifecycle(self):
         
        class TestApp(BaseApp):
            def on_msg_in(self, message):
                msg_type = message.get_field(35)
                if msg_type == MsgType.Logon:
                    self.send(Message(
                        (35,MsgType.Logon),
                        (98, message.get_field(98)), # Encrypt is reflected
                        (108, message.get_field(108)) # HeartBeat is reflected 
                    ))
                elif msg_type == b'APP1':
                    self.send(Message((35,b'APP2')))
                
         
        msp = MockSocketPair()
        app = stack( 
            ( AcceptorSessionApp, 
                dict(socket = msp.a, we=b"ACCEPTOR")
            ), 
            ( TestApp, dict() ) 
        )
        
        msp.b.sendall(self.initiator_logon_msg().to_buf())
        
        self.check_data(
            msp, 
            [self.TestDatum(
                test=self,
                expected_fields=(
                    (35,MsgType.Logon),
                ),
                action=self.add_messages(msp, 
                    Message(
                        (35,b'APP1'),
                        (34,b'2'),
                    )
                )
            )]
        )
        
        self.check_data(
            msp,
            [self.TestDatum(
                test=self,
                expected_fields=(
                    (35,b'APP2'),
                ),
                action=self.add_messages(msp,
                    Message(
                        (35, MsgType.Logout),
                        (34,b'3'),
                    )
                )
            )]
        )
        
        app.wait()
    
class TestInitiatorSessionApp(unittest.TestCase, DataCheckMixIn):
    
    def test_fix42_logon(self):
        msp = MockSocketPair()
        app = InitiatorSessionApp(
            socket = msp.a, 
            username = b"user", 
            password=b"pass", 
            we=b"WE", 
            you=b"YOU", 
            reconnect=False,
            reset_seq_nums=True,
            heartbeat_time=1000, # easier for debugging
            send_period=0.001, # faster test
            begin_string=b'FIX.4.2',
            app_ver_id=None,
        )
        
        logon = six.next(Message.from_socket(msp.b, begin_string=b'FIX.4.2'))
        self.assertEqual(b'A', logon.get_field(35))
        self.assertRaises(TagNotFound, logon.get_field, 1137)
        msp.b.close()
        app.wait()
        
    def test_extra_logon_fields(self):
        msp = MockSocketPair()
        app = InitiatorSessionApp(
            socket = msp.a, 
            username = b"user", 
            password=b"pass", 
            we=b"WE", 
            you=b"YOU", 
            reconnect=False,
            reset_seq_nums=True,
            heartbeat_time=1000, # easier for debugging
            send_period=0.001, # faster test
            extra_logon_fields_fun=lambda _: Message((1000000, b'EXTRA'))
        )
         
        logon = six.next(Message.from_socket(msp.b))
        self.assertEqual(b'A', logon.get_field(35))
        self.assertEqual(b'EXTRA', logon.get_field(1000000))
        msp.b.close()
        app.wait()
        

    def test_do_not_reconnect(self):
        
        class TestApp(BaseApp):
            def __init__(self, *args, **kwargs):
                
                self.msg_counter = 0
                super(TestApp, self).__init__(*args, **kwargs)
                
            def on_msg_in(self, message):
                self.msg_counter += 1
                
                msg_type = message.get_field(35)
                
                if msg_type == MsgType.Logon:
                    pass
                elif msg_type == b'APP1':
                    self.send(Message((35,b'APP2')))
                elif msg_type == b'STOP':
                    self.lower_app.force_stop()
                
        class MockSocketMgr(object):
            def __init__(self):            
                self.socket_pairs = [MockSocketPair(), MockSocketPair()]
                self.socket_index = 0
            
            def __call__(self):
                sp = self.socket_pairs[self.socket_index]
                self.socket_index += 1
                return sp.a # socket for app 
        
        mock_socket_mgr = MockSocketMgr()
        
        app = stack( 
            ( InitiatorSessionApp, dict(
                socket_klass = mock_socket_mgr, 
                username = b"user", 
                password=b"pass", 
                we=b"WE", 
                you=b"YOU", 
                reconnect_time=0, # Just to fail early
                reset_seq_nums=True, # makes it easier to write the test
                heartbeat_time=1000, # easier for debugging
                send_period=0.001 # faster test
            ) ), 
            ( TestApp, dict() )
        )

        msp = mock_socket_mgr.socket_pairs[0]
        
        TEST_DATA_1 = [
            self.TestDatum(
                test=self,
                expected_fields=((35,b"A"), (34,b"1")),
                action=self.add_messages(msp,
                    Message((35,b'A'), (34,b'1')),
                    Message((35,b'APP1'), (34,b'2'))
                )
            ),
        ]
        
        self.check_data(msp, TEST_DATA_1)
        
        TEST_DATA_2 = [
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP2"), (34,b"2")),
                action=self.add_error(msp, Exception("injected exception"))
            ),
        ]

        self.check_data(msp, TEST_DATA_2)
        
        self.assertEqual(2, app.upper_app.msg_counter) # received logon and APP1
        
        msp = mock_socket_mgr.socket_pairs[1]
        msp.b.sendall(
            Message((35, b'A'), (34, b'1')).to_buf()
        )
        msp.b.sendall(
            Message((35, b'STOP'), (34, b'2')).to_buf()
        )
        
        app.wait(1)
        
        self.assertEqual(1, mock_socket_mgr.socket_index) # Second socket not used
        self.assertEqual(2, app.upper_app.msg_counter) # No extra messages received
    
    def test_reset_counters(self):

        class TestApp(BaseApp):
            def __init__(self, test, *args, **kwargs):
                self.test = test
                self.expected_msg_types = [b'A', b'APP1', b'A', b'APP3', b'STOP']

                super(TestApp, self).__init__(*args, **kwargs)
                
            def on_msg_in(self, message):
                msg_type = message.get_field(35)
                self.test.assertEqual(self.expected_msg_types[0], msg_type)
                self.expected_msg_types = self.expected_msg_types[1:]
                
                if MsgType.Logon == msg_type:
                    pass
                    
                elif b'APP1' == msg_type:
                    self.send(Message(
                        (35,b'APP2')
                    ))

                elif b'APP3' == msg_type:
                    self.send(Message(
                        (35,b'APP4')
                    ))
                    
                elif b'STOP' == msg_type: 
                    self.lower_app.force_stop()
        
        class MockSocketMgr(object):
            def __init__(self):            
                self.socket_pairs = [MockSocketPair(), MockSocketPair()]
                self.socket_index = 0
            
            def __call__(self):
                sp = self.socket_pairs[self.socket_index]
                self.socket_index += 1
                return sp.a # socket for app 
        
        mock_socket_mgr = MockSocketMgr()
                    
        app = stack( 
            ( InitiatorSessionApp, dict(
                socket_klass = mock_socket_mgr, 
                username = b"user", 
                password=b"pass", 
                we=b"WE", 
                you=b"YOU", 
                reconnect=True,
                reconnect_time=0, 
                reset_seq_nums=True,
                heartbeat_time=1000, # easier for debugging
                send_period=0.001 # faster test
            ) ), 
            ( TestApp, dict(test=self) )
        )

        msp = mock_socket_mgr.socket_pairs[0]
        
        TEST_DATA_1 = [
            self.TestDatum(
                test=self,
                expected_fields=((35,b"A"), (34,b"1")),
                action=self.add_messages(msp,
                    Message((35,b'A'), (34,b'1')),
                    Message((35,b'APP1'), (34,b'2'))
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP2"), (34,b"2")),
                action=self.add_error(msp, Exception("injected exception"))
            )
        ]
        
        self.check_data(msp, TEST_DATA_1)
        
        msp = mock_socket_mgr.socket_pairs[1]
        
        TEST_DATA_2 = [
            self.TestDatum(
                test=self,
                expected_fields=((35,b"A"), (34,b"1")),
                action=self.add_messages(msp, 
                    Message((35,b'A'),(34,b'1')),
                    Message((35,b'APP3'),(34,b'2')),
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP4"), (34,b"2")),
                action=self.add_messages(msp, Message((35, b"STOP"),(34, b"3")))
            )
        ]
        
        self.check_data(msp, TEST_DATA_2)
        
        app.wait(1)
        
    def test_extra_header_fields(self):
        
        class TestApp(BaseApp):
               
            def on_msg_in(self, message):
                
                msg_type = message.get_field(35)
                
                if msg_type == MsgType.Logon:
                    pass
                elif msg_type == b'APP1':
                    self.send(Message((35,b'APP2')))
                elif msg_type == b'STOP':
                    self.lower_app.force_stop()
                    
        msp = MockSocketPair()
        
        app = stack( 
            ( InitiatorSessionApp, dict(
                socket = msp.a, 
                username = b"user", 
                password=b"pass", 
                we=b"WE", 
                you=b"YOU",
                reconnect=True,
                heartbeat_time=1000, # easier for debugging
                send_period=0.001, # faster test
                extra_header_fields_fun=lambda message: Message((1000,b'ADDED')) if message.get_field(35) == b'APP2' else Message()
            ) ), 
            ( TestApp, dict() )
        )
        
        TEST_DATA = [
            self.TestDatum(
                test=self,
                expected_fields=((35, MsgType.Logon),),
                action=self.add_messages( msp,
                    Message((35,b'A'),(34,b'1')),
                    Message((35,b'APP1'),(34,b'2'))
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b'APP2'),(1000,b'ADDED')),
                action=self.add_messages(msp,
                    Message((35,b'STOP'),(34,b'3'))
                )
            )
        ]
        
        self.check_data(msp, TEST_DATA)
        app.wait(1)

    
    def test_normal_behavior_and_message_consistency(self):
        
        class MyMasterThread(InitiatorSessionApp.MasterThread):
            def __init__(self, *args, **kwargs):
                super(MyMasterThread, self).__init__(*args, **kwargs)
                # Inject message before even logging in, simulating attempts to send messages when reconnecting
                self.send(Message(
                    (35,b"BEFORE_LOGIN")
                ))   
                
        class MyInitiatorSessionApp(InitiatorSessionApp):
            MasterThread = MyMasterThread 
                
        class TestApp(BaseApp):
            def __init__(self, test, *args, **kwargs):
                self.test = test
                self.expected_msg_types = [b'A', b'APP1', b'2', b'STOP']
                self.msg_not_rcvd_types = collections.Counter()
                super(TestApp, self).__init__(*args, **kwargs)
                
            def on_msg_in(self, message):
                msg_type = message.get_field(35)
                self.test.assertEqual(self.expected_msg_types[0], msg_type)
                self.expected_msg_types = self.expected_msg_types[1:]
                
                if MsgType.Logon == msg_type:
                    pass
                    
                elif b'APP1' == msg_type:
                    self.send(Message(
                        (35,b'APP2')
                    ))
                    self.send(Message(
                        (35,b'APP3')
                    ))
                    self.send(Message(
                        (35,b'APP4')
                    ))

                elif b'STOP' == msg_type: 
                    self.lower_app.force_stop()
            
            def on_msg_not_rcvd(self, message):
                self.msg_not_rcvd_types[message.get_field(35)] += 1  


        msp = MockSocketPair()
        
        app = stack( 
            ( MyInitiatorSessionApp, dict(
                socket = msp.a, 
                username = b"user", 
                password=b"pass", 
                we=b"WE", 
                you=b"YOU",
                reconnect=True,
                heartbeat_time=1000, # easier for debugging
                send_period=0.001 # faster test

            ) ), 
            ( TestApp, dict(test=self) )
        )
        
        TEST_DATA = [
            self.TestDatum(
                test=self,
                expected_fields=((35,MsgType.Logon),(34,b'1'),(1137,b'9')), # 1137=9 means FIXT 5.0sp2
                action=self.compose(
                    self.add_messages(msp,Message((35,b'A'),(34,b'1'))),
                    self.add_error(msp, ssl.SSLError("Simulating a socket that timed out"))
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,MsgType.TestRequest),(34,b'2')),
                action=self.add_messages(msp,Message((35,b'APP1'),(34,b'2')))
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP2"),(34,b'3'))
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP3"),(34,b'4'))
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35,b"APP4"),(34,b'5')),
                action=self.add_messages(msp, 
                    Message((35,b'APP1'),(34,b'2'),(43,b'Y')), # Message is ignored
                    Message((35,MsgType.ResendRequest),(34,b'3'),(7,b'4'),(16,b'0'))
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35, MsgType.SequenceReset),(34,b'4'),(36,b'6'),(123,b'Y')),
                action=self.add_messages(msp,
                    Message((35,b'APP1'),(34,b'5')) # intentionally skipped one, testing sequence number going backwards!
                )
            ),
            self.TestDatum(
                test=self,
                expected_fields=((35, MsgType.ResendRequest), (34,b'6'), (7,b'4'), (16,b'0')),
                action=self.add_messages(msp, Message((35,b"STOP"),(34,b'4')))
            )
        ]
        
        self.check_data(msp, TEST_DATA)
        
        app.wait(1)
       
        self.assertDictEqual(
            collections.Counter({b'APP3':1, b'APP4':1, b'BEFORE_LOGIN':1}), 
            app.upper_app.msg_not_rcvd_types
        )
    
